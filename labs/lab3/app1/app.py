import os
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc

database_uri = 'postgresql://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(
    dbuser=os.environ['DBUSER'],
    dbpass=os.environ['DBPASS'],
    dbhost=os.environ['DBHOST'],
    dbname=os.environ['DBNAME']
)

app = Flask(__name__)
app.config.update(
    SQLALCHEMY_DATABASE_URI=database_uri,
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)
# initialize the database connection
db = SQLAlchemy(app)

# @app.route("/")
# def hello_world():
#     return "Hello Red Hat Summit!"

class BlogPost(db.Model):
    """Simple database model for blog posts."""

    __tablename__ = 'blogposts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    body = db.Column(db.String(240))

    def __init__(self, title=None, body=None):
        self.title = title
        self.body = body

def lazy_create_db():
    e = db.engine
    if (not e.dialect.has_table(e, BlogPost.__tablename__)):
        db.create_all()
        db.session.commit()

@app.route('/')
def view_posts():
    lazy_create_db()
    blogposts = BlogPost.query.all()
    return render_template('blog_posts.html', blogposts=blogposts)

@app.route('/postblog', methods=['GET'])
def view_blog_post_form():
    return render_template('blog_post.html')

@app.route('/postblog', methods=['POST'])
def save_post():
    title = request.form.get('title')
    body = request.form.get('body')

    post = BlogPost(title, body)
    db.session.add(post)
    db.session.commit()

    return render_template(
        'post_confirmation.html', title=title, body=body)

if __name__ == "__main__":
    app.run("0.0.0.0", debug=True)

