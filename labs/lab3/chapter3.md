[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 3: Using Our Application Streams
## Introduction

In this lab we will deploy a containerized database and create front-end containers for our Flask Application Streams.

#TODO server and username ->
This lab should be performed on `workstation.example.com` as `root` unless otherwise instructed.

Expected completion: 15-20 minutes #TODO fix times

NOTE: much of the superflous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## Getting a database

Red Hat provides a significant number of containerized applications in the [Red Hat Container Catalog](https://access.redhat.com/containers/) from both Red Hat and Third Parties. 
You should definitely consider this resource for high quality, trustable, easy to access packaged applications. 
For this lab, we will be leveraging one of the PostgreSQL containers as the backend for our ACME software.

Browse to the [Red Hat Container Catalog](https://access.redhat.com/containers/).

Enter "postgresql" in the search bar.
Select ["rhscl/postgresql-96-rhel7"](https://access.redhat.com/containers/?tab=overview#/registry.access.redhat.com/rhscl/postgresql-96-rhel7) repository.
Select "Get This Image" tab.
Scroll down to "Using podman login" section.

However, for the sake of the internet performance at a conference, we are actually going to pull the container from our local repository.

Need to pull and run as root since podman doesn't yet support rootless port binding.
```bash
$ sudo -s
# podman pull core.example.com:5000/postgresql-96-rhel7
# podman run -d \
    --name postgresql_database \
    -e POSTGRESQL_USER=user \
    -e POSTGRESQL_PASSWORD=pass \
    -e POSTGRESQL_DATABASE=db \
    -p 5432:5432 \
    core.example.com:5000/postgresql-96-rhel7
```

As an interesting aside, if you notice, this is a PostgreSQL database running based on a RHEL7 container.
As you may or may not know, you can, generally, mix and match containers across RHEL versions. 
For specific scenarios, you should consult the [Customer Portal](https://access.redhat.com).

In order to verify the database is working, let's connect to it. 
However, the PostgreSQL client isn't installed by default.
So, let's go get it.
We are going to use the PostgreSQL module and the ```client``` profile.

```bash
$ yum install @postgresql/client
```
You should see messages about installing the ```postgresql``` rpm, the ```postgresql/client``` profile, and enabling the ```postgresql:10``` module stream.

Now we can connect and make sure the database is really running.
```bash
$ psql -h localhost -p 5432 -d db -U user
db=> 
```

## Create Front-End Container Python27

For the sake of simplicity we have created a very simple Python Flask application in the labs/lab3/app1 directory.
Now we will create a OCI file to run our application in.

```
#use a local base image
FROM core.example.com:5000/rhel8

#use the local copy of the repos for performance
RUN rm /etc/yum.repos.d/*
ADD rhel8.repo /etc/yum.repos.d/redhat.repo 

# python27 module should be installed as a modular runtime dependency
RUN yum -y module install flask:py27 && yum clean all

#copy our app in to the container
ADD * /app/
ADD templates /app/templates

EXPOSE 5000

CMD [ "/usr/bin/python2", "/app/flask-hello.py" ]
```

Save this file as `OCIfile`

Now we build and run the application using podman.
Note: We need to build and run as root since podman doesn't yet support rootless port binding. #TODO the ip in the command below might be wrong, needs testing on the actual env

```bash
$ sudo podman build -t flask-frontend --file=OCIfile .
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="10.0.2.1" \
    -e DBNAME="db" \
    flask-frontend
```
You can now open a web browser on your computer and connect to the new application using the ip that you have been ssh'ing to the machine with. 
For example: ```http://workstation.example.com:5000``` #TODO will the host file have the ip mappings in it?

Go ahed and play around with the application a bit. 
When you are done you can move to the next lab. #TODO link

## Create Front-End Container Python36 #TODO

* create schema & user for app2 (flask:py36) #TODO
```bash
$ psql -h localhost -p 5432 -d db -U user
db=> 
```
* repeat for flask:py36
