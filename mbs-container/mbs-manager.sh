#!/bin/bash

mkdir -p ./modulebuild && \
podman run -it --rm \
    --tmpfs /tmp \
    --tmpfs /run \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    --privileged \
    -v "$PWD":/wd \
    -v "$PWD/modulebuild":/root/modulebuild \
    -v "$PWD/rpms":/root/rpms \
    --workdir /wd \
    core.example.com:5000/appstream-lab/mbs-container "$@"
